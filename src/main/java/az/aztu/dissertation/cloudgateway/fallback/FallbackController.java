package az.aztu.dissertation.cloudgateway.fallback;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class FallbackController {

    @GetMapping("/auth/fallback")
    public Mono<String> getAccountFallbackMessage() {
        return Mono.just("No response from Auth Service and will be back shortly");
    }

    @GetMapping("/accounts/fallback")
    public Mono<String> getUserFallbackMessage() {
        return Mono.just("No response from Account Service and will be back shortly");
    }
}
